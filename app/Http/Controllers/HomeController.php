<?php

namespace App\Http\Controllers;

use App\Artikel;
use App\Kategori;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index(){

        $items = Artikel::with(['kategori'])->paginate(3);
        return view('pages.home',[
            'items' => $items,
            'artikel' => Artikel::count(),
            'kategori' => Kategori::count(),
        ]);
    }

    public function search(Request $request){
        $id_kategori = $request->input('id_kategori');

        $query = Artikel::with(['kategori']);

        if ($id_kategori) {
            $query->where('id_kategori', $id_kategori);
        }

        $items = $query->paginate(3);

        return view('pages.home', [
            'items' => $items,
            'artikel' => Artikel::count(),
            'kategori' => Kategori::count(),
        ]);
    }

}
