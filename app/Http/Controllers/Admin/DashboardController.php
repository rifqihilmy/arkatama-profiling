<?php

namespace App\Http\Controllers\Admin;

use App\Artikel;
use App\Kategori;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function index(){
        return view('pages.admin.dashboard',[
            'artikel' => Artikel::count(),
            'kategori' => Kategori::count(),
        ]);
    }
}
