<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Artikel extends Model
{
    protected $table = 'artikel';
    protected $fillable = [
        'judul', 'about', 'image', 'id_kategori'
    ];

    protected $hidden = [

    ];

    public function kategori(){
        return $this->belongsTo( Kategori::class, 'id_kategori', 'id' );
    }
}
