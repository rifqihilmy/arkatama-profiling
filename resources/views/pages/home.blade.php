@extends('layouts.app')
@section('title','Homepage')
@section('content')
<header class="text-center">
  <h1>
    Berita Terbaru
    <br />
    Dapatkan Informasi Sekarang
  </h1>
  <p class="mt-3">
    Anda akan mendapatkan berita
    <br />
    momen-momen menarik yang belum pernah Anda lihat sebelumnya
  </p>  
    <a href="#" class="btn btn-get-started px-4 mt-4">
      Get Started
    </a>
  </header>
  <main>
    <div class="container">
      <section class="section-stats row justify-content-center" id="stats">
        <div class="col-3 col-md-2 stats-detail">
          <h2>{{ $artikel }}</h2>
          <p>Artikel</p>
        </div>
        <div class="col-3 col-md-2 stats-detail">
          <h2>{{ $kategori }}</h2>
          <p>Kategori</p>
        </div>
      </section>
    </div>
    <section class="section-popular" id="popular">
      <div class="container">
        <div class="row">
          <div class="col text-center section-popular-heading">
            <h2>Telusuri Berita Populer</h2>
            <p>
              Hal-hal yang belum pernah Anda baca
              <br />
              sebelumnya di dunia ini
            </p>
          </div>
        </div>
      </div>
    </section>
    <section class="section-testimonials-content" id="artikel">
      <div class="container">
        <form action="{{ route('search') }}" method="GET">
          <div class="input-group mb-3">
              <select class="custom-select" name="id_kategori">
                  <option value="" selected>Pilih Kategori</option>
                  @foreach ($items as $item)
                      <option value="{{ $item->kategori->id }}">{{ $item->kategori->jenis }}</option>
                  @endforeach
              </select>
              <div class="input-group-append">
                  <button class="btn btn-primary" type="submit">Cari</button>
              </div>
          </div>
        </form>
      </div>
        <div class="container">
          <div
            class="section-popular-travel row justify-content-center match-height"
          >
          @foreach ($items as $item)
            <div class="col-sm-6 col-md-6 col-lg-4">
              <div class="card card-testimonial text-center">
                <div class="testimonial-content">
                  <h3 class="mb-4">{{ $item->judul }}</h3>
                  <img
                    src="{{ url($item->count() ? Storage::url($item->image) : '') }}"
                    alt=""
                    class="mb-4 rounded-circle"
                    style="width: 150px; height: 150px;"
                  />
                  <p class="testimonials">{{ Str::limit($item->about, 100) }}</p>
                </div>
                <hr />
                <p class="trip-to mt-2">{{ $item->kategori->jenis }}</p>
                <hr />
                <p class="trip-to mt-2"><a href="#" class="btn btn-get-started px-4 mt-4 mx-1" data-toggle="modal" data-target="#modal{{ $item->id }}">
                  Detail
                </a></p>
              </div>
            </div>
            <div class="modal fade" id="modal{{ $item->id }}" tabindex="-1" role="dialog" aria-labelledby="modalLabel{{ $item->id }}" aria-hidden="true">
              <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
                  <div class="modal-content">
                      <div class="modal-header">
                          <h5 class="modal-title" id="modalLabel{{ $item->id }}">{{ $item->judul }} ({{ $item->kategori->jenis }})</h5>
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                          </button>
                      </div>
                      <div class="modal-body">
                          <img src="{{ url(Storage::url($item->image)) }}" alt="{{ $item->judul }}" class="img-fluid mb-3">
                          <p>{{ $item->about }}</p>
                          <p class="text-right"> {{ $item->created_at->format('d F Y') }}</p>
                      </div>
                  </div>
              </div>
          </div>          
          @endforeach
          {{ $items->links() }}
          </div>
        </div>
      </section>
  </main>
@endsection