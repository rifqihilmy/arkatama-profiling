<!-- Semantic elements -->
    <!-- https://www.w3schools.com/html/html5_semantic_elements.asp -->

    <!-- Bootstrap navbar example -->
    <!-- https://www.w3schools.com/bootstrap4/bootstrap_navbar.asp -->
    <div class="container">
        <nav class="row navbar navbar-expand-lg navbar-light bg-white">
          <a class="navbar-brand" href="{{ route('home') }}">
            {{-- <img src="frontend/images/logo.png" alt="" /> --}}
            <p>MiniBlog</p>
          </a>
          <button
            class="navbar-toggler navbar-toggler-right"
            type="button"
            data-toggle="collapse"
            data-target="#navb"
          >
            <span class="navbar-toggler-icon"></span>
          </button>
  
          <!-- Menu -->
          <div class="collapse navbar-collapse" id="navb">
            <ul class="navbar-nav ml-auto mr-3">
              <li class="nav-item mx-md-2">
                <a class="nav-link active" href="{{ route('home') }}">Home</a>
              </li>
              <li class="nav-item mx-md-2">
                <a class="nav-link" href="#artikel" >Artikel</a>
              </li>
            </ul>
            @guest
            <form class="form-inline my-2 my-lg-0 d-none d-md-block">
              <button class="btn btn-login btn-navbar-right my-2 my-sm-0 px-4" onclick="event.preventDefault(); location.href='{{ url('login') }}';">
                Masuk
              </button>
            </form>
            @endguest
            @auth
            <form class="form-inline my-2 my-lg-0 d-none d-md-block" action="{{  url('logout') }}" method="POST">
              @csrf
              <button class="btn btn-login btn-navbar-right my-2 my-sm-0 px-4" type="submit">
                  Keluar
              </button>
            </form>
            @endauth
          </div>
        </nav>
      </div>